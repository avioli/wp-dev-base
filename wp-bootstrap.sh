#!/usr/bin/env bash

test -z "$WEBSITE_NAME" && { echo "No WEBSITE_NAME env."; exit 1; }
test -z "$WEBSITE_DATABASE_NAME" && { echo "No WEBSITE_DATABASE_NAME env."; exit 1; }
test -z "$MYSQL_USER_NAME" && { echo "No MYSQL_USER_NAME env."; exit 1; }
test -z "$MYSQL_USER_PASS" && { echo "No MYSQL_USER_PASS env."; exit 1; }

if test ! -d wp; then
	if test -f ~/.config/wp/wordpress-base-install; then
		WP_BASE_DIR=$(head -n1 ~/.config/wp/wordpress-base-install)
		## Replace ~ with $HOME
		WP_BASE_DIR="${WP_BASE_DIR/#\~/$HOME}"
		git clone "$WP_BASE_DIR" wp
	else
		git clone git@bitbucket.org:ionata/wordpress-base-install.git wp
	fi
fi

CONFIG_FILE=wp-config.php

cat <<EOF > $CONFIG_FILE
<?php
// Generated WordPress config file by wp-bootstrap.sh

//define( 'ENVIRONMENT', 'production' );
define( 'WP_HOME', 'http://$WEBSITE_NAME.localhost:8080' );
//define( 'REPLACE_RENDERED_HOME_URL_IN_HTML', 'http://$WEBSITE_NAME.com.au/' );

require_once(__DIR__ . '/wp-dev.php');
define('WP_DEBUG', false);

// ** MySQL settings ** //
/** The name of the database for WordPress */
define('DB_NAME', '$WEBSITE_DATABASE_NAME');

/** MySQL database username */
define('DB_USER', '$MYSQL_USER_NAME');

/** MySQL database password */
define('DB_PASSWORD', '$MYSQL_USER_PASS');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

EOF

curl -sSL "https://api.wordpress.org/secret-key/1.1/salt/" >> $CONFIG_FILE

cat <<'EOF' >> $CONFIG_FILE

$table_prefix = 'wp_';

define('WPLANG', '');




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

//add_action( 'admin_enqueue_scripts', function() { wp_deregister_style( 'open-sans' ); wp_register_style( 'open-sans', 'no-open-sans.css', array(), null ); }, 1, 0 );
EOF

if test -d wp-content; then
	echo "Found wp-content dir."
	echo
	echo "DONE."
else
	if test -n "$1"; then
		echo "Cloning $1..."

		git clone "$1" wp-content

		echo
		echo "DONE."
	else
		echo "Now clone your repo into wp-content. Eg. 'git clone git@bitbucket.org:ionata/$WEBSITE_NAME.git wp-content'"
	fi
fi

echo "You may want to edit wp-config.php to change the \$table_prefix variable and maybe change/enable REPLACE_RENDERED_HOME_URL_IN_HTML."
