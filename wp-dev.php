<?php
// START
/**
 * Custom settings for local dev env. "Evo Stamatov" <evo@ionata.com.au>
 *
 * version 1.0.1
 *
 * 2015-04-23
 *   - Check if WP_CORE_DIR_NAME is empty
 *   - Remove rhs slash from REPLACE_RENDERED_HOME_URL_IN_HTML's replacement - WP_HOME.'/'
 *
 * 2014-11-04
 *   - Simplify pre-defined checks
 *   - Get rid of open-sans style
 *
 * 2014-07-08
 *   - Check for pre-defined values
 *   - Add REPLACE_RENDERED_HOME_URL_IN_HTML
 *
 * 2014-05-19
 *   - Initial version
 */ 

function __define( $name, $value ) {
	if ( ! defined( $name ) ) {
		define( $name, $value );
	}
}

//__define( 'ENVIRONMENT', 'DEVELOPMENT' );
__define( 'ENVIRONMENT', 'development' );

// WP_CORE_DIR_NAME env should be sans trailing slash
__define( 'WP_CORE_DIR_NAME', getenv('WP_CORE_DIR_NAME') ? getenv('WP_CORE_DIR_NAME') : 'wp' );
$wp_core_dir_path = '/' . WP_CORE_DIR_NAME;
if ($wp_core_dir_path === '/') {
	$wp_core_dir_path = '';
}

// WP_CONTENT_DIR_NAME env should be sans trailing slash
__define( 'WP_CONTENT_DIR_NAME', getenv('WP_CONTENT_DIR_NAME') ? getenv('WP_CONTENT_DIR_NAME') : 'wp-content' );

// WP_HOME env should be sans trailing slash
__define( 'WP_HOME', getenv('WP_HOME') ? getenv('WP_HOME') : 'http://localhost:8080' );

__define( 'WP_SITEURL', WP_HOME . $wp_core_dir_path );
__define( 'WP_CONTENT_DIR', __DIR__ . '/' . WP_CONTENT_DIR_NAME );
__define( 'WP_CONTENT_URL', WP_HOME . '/' . WP_CONTENT_DIR_NAME );
//__define( 'UPLOADS', '../wp-content/uploads' ); // resolves to ABSPATH . /

__define( 'AUTOMATIC_UPDATER_DISABLED', true );
// __define( 'DISABLE_WP_CRON', true );
__define( 'DISALLOW_FILE_EDIT', true );

__define( 'AUTOSAVE_INTERVAL', 160 );
__define( 'WP_POST_REVISIONS', false ); // or a number
__define( 'CONCATENATE_SCRIPTS', false );
__define( 'CONCATENATE_STYLES', false );
//__define( 'EMPTY_TRASH_DAYS', 0 );
//__define( 'ENABLE_CACHE', true );

__define( 'ABSPATH', __DIR__ . $wp_core_dir_path . '/' );

if ( defined('REPLACE_RENDERED_HOME_URL_IN_HTML') && strlen(REPLACE_RENDERED_HOME_URL_IN_HTML) > 0 ) {
	ob_start( function( $buf = '' ) {
	    //error_log(strpos($buf,"http://wildcare.dev.com/"));
		return str_replace( REPLACE_RENDERED_HOME_URL_IN_HTML, WP_HOME, $buf );
	});
	// WordPress calls wp_ob_end_flush_all() when shutting down. In wp-includes/functions.php
}

/*
 NOTE: You can add the following to the end of your wp-config.php on an WP3.8+ install to remove the remote Google Fonts Open Sans request.

add_action( 'admin_enqueue_scripts', function() { wp_deregister_style( 'open-sans' ); wp_register_style( 'open-sans', 'no-open-sans.css', array(), null ); }, 1, 0 );

 */

// END.
