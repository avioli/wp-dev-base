<?php
// WordPress view bootstrapper
if ( !defined('WP_CORE_DIR_NAME') )
	define('WP_CORE_DIR_NAME', getenv('WP_CORE_DIR_NAME') ? getenv('WP_CORE_DIR_NAME') : 'wp');

define('WP_USE_THEMES', true);
require(dirname( __FILE__ ) . '/' . WP_CORE_DIR_NAME . '/wp-blog-header.php');
